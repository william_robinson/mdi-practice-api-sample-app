(function () {
    angular.module('mdi-practice-api-sample-app.auth', ['angular-jwt'])
        .config(["jwtOptionsProvider", "$httpProvider", function (jwtOptionsProvider, $httpProvider) {
            jwtOptionsProvider.config({
                tokenGetter: [function () {
                    return localStorage.getItem('id_token');
                }],
                whiteListedDomains: ["localhost"]
            });

            $httpProvider.interceptors.push('jwtInterceptor');
        }])
        .run(["authManager", function(authManager){
            authManager.checkAuthOnRefresh();

            var tokenMatches = window.location.hash.match(/id_token=([^&]*)/);
            if(tokenMatches){
                localStorage.setItem("id_token", tokenMatches[1]);
            }
        }])
        .constant("AuthConfig", {
            authorizeUrl: "https://warobinson.auth0.com/authorize",
            audience: "http://practiceapi.mdintellesys.com",
            clientId: "Xwc6NwoL24AK32435Yn2pfkr2OXVOUkp",
            logoutUrl: "https://warobinson.auth0.com/v2/logout"
        });
})();