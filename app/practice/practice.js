(function () {
    angular
        .module('mdi-practice-api-sample-app.practice-view', ['ngRoute', "mdi-practice-api-sample-app.auth"])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/practice', {
                templateUrl: 'practice/practice.html',
                controller: 'PracticeCtrl as Practice'
            });
        }])
        .controller('PracticeCtrl', PracticeController);

    PracticeController.$inject = ["AuthSvc", "PatientSvc"]
    function PracticeController(AuthSvc, PatientSvc) {
        var vm = this;
        vm.practices = AuthSvc.GetPracticeAccess();
        vm.loading_details = false;
        vm.SearchPatientsByDrug = SearchPatientsByDrug
        vm.HideDetails = HideDetails;

        function SearchPatientsByDrug(practiceId, drug){
            vm.loading_details = true;
            var practice = vm.practices.find(function(practice){
                return practice.id == practiceId;
            });
            PatientSvc.GetPatientsByDrug(practiceId, drug).then(function(list){
                vm.loading_details = false;
                practice.patient_list = list;
            });
        }

        function HideDetails(practice){
            practice.patient_list = "";
        }
    }
})();