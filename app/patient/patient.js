(function () {
    angular
        .module('mdi-practice-api-sample-app.patient-view', ['ngRoute', "mdi-practice-api-sample-app.auth"])
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/patient', {
                templateUrl: 'patient/patient.html',
                controller: 'PatientCtrl as Patient'
            });
        }])
        .controller('PatientCtrl', PatientController);

    PatientController.$inject = ["AuthSvc", "PatientSvc"]
    function PatientController(AuthSvc, PatientSvc) {
        var vm = this;
        vm.practices = AuthSvc.GetPracticeAccess();
        vm.loading_details = false;
        vm.GetDetails = GetDetails;
        vm.HideDetails = HideDetails;

        function GetDetails(practiceId, patientId){
            vm.loading_details = true;
            var practice = vm.practices.find(function(practice){
                return practice.id == practiceId;
            });
            PatientSvc.GetPatientDetails(patientId, practiceId).then(function(details){
                vm.loading_details = false;
                practice.patient_details = details;
            });
        }

        function HideDetails(practice){
            practice.patient_details = "";
        }
    }
})();