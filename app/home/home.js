'use strict';

angular
    .module('mdi-practice-api-sample-app.view1', ['ngRoute'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'home/home.html',
            controller: 'HomeCtrl as Home'
        });
    }])
    .controller('HomeCtrl', HomeController);

HomeController.$inject = ["AuthSvc", "PingSvc"]
function HomeController(AuthSvc, PingSvc){
    var vm = this;
    vm.SecurePing = SecurePing;
    vm.PatientPing = PatientPing;
    vm.PracticePing = PracticePing;
    vm.PatientOrPracticePing = PatientOrPracticePing;
    vm.ReadClaims = ReadClaims;
    vm.Ping = Ping;
    vm.claims = "";
    vm.pingStatus = "";
    vm.loginStatus = "";
    vm.isLoggedIn = AuthSvc.IsAuthenticated();
    vm.isPractice = AuthSvc.HasRole("practice");
    vm.isPatient = AuthSvc.HasRole("patient");


    function SecurePing(){
        vm.pingStatus = "Sending ping...";
        PingSvc.Secure().then(function(result){
            vm.pingStatus = result;
        });
    }

    function Ping(){
        vm.pingStatus = "Sending ping...";
        PingSvc.Public().then(function(result){
            vm.pingStatus = result;
        });
    }

    function PatientPing(){
        vm.pingStatus = "Sending ping...";
        PingSvc.Patient().then(function(result){
            vm.pingStatus = result;
        });
    }

    function PracticePing(){
        vm.pingStatus = "Sending ping...";
        PingSvc.Practice().then(function(result){
            vm.pingStatus = result;
        });
    }

    function PatientOrPracticePing(){
        vm.pingStatus = "Sending ping...";
        PingSvc.PatientOrPractice().then(function(result){
            vm.pingStatus = result;
        });
    }

    function ReadClaims(){
        PingSvc.GetClaims().then(function(claims){
            vm.claims = claims;
        })
    }
}

