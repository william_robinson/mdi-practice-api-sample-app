(function () {
    angular
        .module("mdi-practice-api-sample-app.practiceAPI", [])
        .constant("PracticeApiConfig", {
            UrlBase: "http://localhost:57737/v1/api/"
        });
})();