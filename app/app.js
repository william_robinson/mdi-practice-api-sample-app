'use strict';

// Declare app level module which depends on views, and components
angular
    .module('mdi-practice-api-sample-app', [
        'ngRoute',
        'mdi-practice-api-sample-app.auth',
        'mdi-practice-api-sample-app.navigation',
        'mdi-practice-api-sample-app.view1',
        'mdi-practice-api-sample-app.patient-view',
        'mdi-practice-api-sample-app.practice-view',
        'mdi-practice-api-sample-app.version',
        'mdi-practice-api-sample-app.practiceAPI'
    ])
    .config(['$locationProvider', '$routeProvider'
        , function($locationProvider, $routeProvider) {
        $locationProvider.hashPrefix('!');
        $routeProvider.otherwise({redirectTo: RedirectToLogin});

        function RedirectToLogin(){
            return '/home';
        }
    }]);
