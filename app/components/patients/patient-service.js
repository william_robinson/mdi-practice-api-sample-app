(function () {
    angular
        .module("mdi-practice-api-sample-app.practiceAPI")
        .factory("PatientSvc", PatientService);

    PatientService.$inject = ["$http", "PracticeApiConfig"];
    function PatientService($http, apiConfig){
        return {
            GetPatientDetails: GetPatientDetails,
            GetPatientsByDrug: GetPatientsByDrug
        }

        function GetPatientDetails(patientId, practiceId){
            return $http.get(apiConfig.UrlBase + "patients/" + patientId + "/practices/" + practiceId).then(function(response){
                return response.data;
            }, function(){
                return "Error:" + response.statusText;
            });
        }

        function GetPatientsByDrug(practiceId, drugName){
            return $http.get(apiConfig.UrlBase + "patients/practices/" + practiceId + "/search?drug=" + drugName).then(function(response){
                return response.data;
            }, function(){
                return {
                    "error": response.statusText
                };
            });
        }
    }

})();