(function () {
    angular
        .module("mdi-practice-api-sample-app.auth")
        .directive("authRole", AuthRoleDirective);

    AuthRoleDirective.$inject = ["AuthSvc"];
    function AuthRoleDirective(AuthSvc){
        return {
            restrict: "A",
            scope: {
                authRole: "@"
            },
            link: function(scope, element){
                var roles = scope.authRole.split(' ');
                var inRole = false;
                angular.forEach(roles, function (role) {
                    if (AuthSvc.HasRole(role)) {
                        inRole = true;
                    }
                });
                if(inRole){
                    element.removeClass("auth-hide");
                } else{
                    element.addClass("auth-hide");
                }
            }
        }
    }
})();