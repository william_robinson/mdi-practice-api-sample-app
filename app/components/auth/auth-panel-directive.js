(function () {
    angular
        .module("mdi-practice-api-sample-app.auth")
        .controller("AuthPanelDirectiveCtrl", AuthPanelDirectiveController)
        .directive("authPanel", AuthPanelDirective);

    function AuthPanelDirective(){
        return {
            restrict: "E",
            scope: {},
            template: `
                    <span>{{ Auth.loginStatus }}</span>
                    <span ng-if="!Auth.IsLoggedIn()">
                        <a href="" ng-click="Auth.GoogleLogin()">Google Login</a> |
                        <a href="" ng-click="Auth.Auth0Login()">Auth0 Login</a>
                    </span>
                    <span ng-if="Auth.IsLoggedIn()">
                        <a href="" ng-click="Auth.Logout()">Logout</a>
                    </span>
                `,
            controller: "AuthPanelDirectiveCtrl as Auth"
        }
    }

    AuthPanelDirectiveController.$inject = ["AuthSvc"];
    function AuthPanelDirectiveController(AuthSvc){
        var vm = this;
        vm.GoogleLogin = GoogleLogin;
        vm.Auth0Login = Auth0Login;
        vm.Logout = Logout;
        vm.IsLoggedIn = IsLoggedIn;

        function IsLoggedIn(){
            return AuthSvc.IsAuthenticated();
        }

        function GoogleLogin(){
            if (!AuthSvc.GoogleLogin()){
                vm.loginStatus = "Error logging in";
            }
        }

        function Auth0Login(){
            if(!AuthSvc.Auth0Login()){
                vm.loginStatus = "Error logging in";
            }
        }

        function Logout(){
            AuthSvc.Logout();
        }
    }
})();