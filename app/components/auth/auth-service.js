(function () {
    angular
        .module('mdi-practice-api-sample-app.auth')
        .factory('AuthSvc', AuthService);

    AuthService.$inject = ["$window", "$location", "AuthConfig", "authManager", "jwtHelper"];
    function AuthService($window, $location, AuthConfig, authManager, jwtHelper){
        return {
            SaveIdToken: SaveIdToken,
            IsAuthenticated: IsAuthenticated,
            GoogleLogin: GoogleLogin,
            Auth0Login: Auth0Login,
            Logout: Logout,
            IsAuthenticated: IsAuthenticated,
            HasRole: HasRole,
            GetPracticeAccess: GetPracticeAccess
        }

        function GetToken(){
            var rawToken = authManager.getToken();
            if(!rawToken){
                return {};
            }
            return jwtHelper.decodeToken(rawToken);
        }

        function HasRole(role){
            if(!authManager.isAuthenticated()){
                return false;
            }
            var token = GetToken();
            if (!(token && token.roles && (token.roles instanceof Array))){
                return false;
            }
            return token.roles.find(function(r){ return r == role });
        }

        function GetPracticeAccess(){
            var token = GetToken();
            if(!token){
                return [];
            }
            return token.practices || [];
        }

        function IsAuthenticated(){
            return authManager.isAuthenticated();
        }

        function SaveIdToken(newToken){
            if(newToken == null){
                localStorage.removeItem("id_token");
            } else{
                localStorage.setItem("id_token", newToken);
            }
        }

       function GoogleLogin(){
            var nonce = CreateNonce();
            $window.location = AuthConfig.authorizeUrl + "?"
                + "&redirect_uri="
                + $location.protocol() + "://" + $location.host() + ($location.port() != "80" ? (":" + $location.port()) : "")
                + "&response_type=token"
                + "&scope=openid roles practices"
                + "&client_id=" + AuthConfig.clientId
                + "&connection=google-oauth2"
                + "&nonce=" + nonce;
            return true;
        }

        function Auth0Login(){
            var nonce = CreateNonce();
            $window.location = AuthConfig.authorizeUrl + "?"
                + "&redirect_uri="
                + $location.protocol() + "://" + $location.host() + ($location.port() != "80" ? (":" + $location.port()) : "")
                + "&response_type=token id_token"
                + "&scope=openid roles practices"
                + "&client_id=" + AuthConfig.clientId
                + "&connection=Username-Password-Authentication"
                + "&nonce=" + nonce;
            return true;
        }

        function Logout(){
            localStorage.removeItem("id_token");
            $window.location = AuthConfig.logoutUrl + "?"
                + "client_id=" + AuthConfig.clientId
                + "&returnTo="
                + $location.protocol() + "://" + $location.host() + ($location.port() != "80" ? (":" + $location.port()) : "");
        }

        function CreateNonce(){
            return "mynonce";
        }
    }
})();