(function () {
    angular
        .module("mdi-practice-api-sample-app.practiceAPI")
        .factory("PingSvc", PingService);

    PingService.$inject = ["$http", "PracticeApiConfig"];
    function PingService($http, apiConfig){
        return {
            Public: Ping,
            Secure: PingSecure,
            Patient: PingPatient,
            Practice: PingPractice,
            PatientOrPractice: PingPatientOrPractice,
            GetClaims: GetClaims
        }

        function Ping(){
            return $http.get(apiConfig.UrlBase + "ping",
                {
                    skipAuthorization: true
                }).then(SuccessResult, FailureResult);
        }

        function PingSecure(){
            return $http.get(apiConfig.UrlBase + "ping/secure")
                .then(SuccessResult, FailureResult);
        }

        function PingPatient(){
            return $http.get(apiConfig.UrlBase + "ping/patient")
                .then(SuccessResult, FailureResult);
        }

        function PingPractice(){
            return $http.get(apiConfig.UrlBase + "ping/practice")
                .then(SuccessResult, FailureResult);
        }

        function PingPatientOrPractice(){
            return $http.get(apiConfig.UrlBase + "ping/patientorpractice")
                .then(SuccessResult, FailureResult);
        }

        function GetClaims(){
            return $http.get(apiConfig.UrlBase + "ping/claims")
                .then(function(response){
                    return response.data;
                }, FailureResult);
        }

        function SuccessResult(response){
            return "Success: " + response.statusText + " - " + response.data;
        }

        function FailureResult(response){
            if (response.status == 401){
                return "Unauthorized: " + response.statusText + " - " + response.data;
            }
            return "Error: " + (response.statusText ? response.statusText : "Unknown error") + " - " + response.data;
        }
    }
})();