(function () {
    angular
        .module("mdi-practice-api-sample-app.navigation", ["mdi-practice-api-sample-app.auth"])
        .directive("navigation", NavigationDirective);

    function NavigationDirective(){
        return{
            restrict: "E",
            template:
                `
                    <div id="auth">
                        <auth-panel></auth-panel>
                    </div>
                    <ul class="menu">
                        <li><a href="#!/view1">Home</a></li>
                        <li auth-role="patient"><a href="#!/patient">Patient</a></li>
                        <li auth-role="practice"><a href="#!/practice">Practice</a></li>
                    </ul>                `
        };
    }
})();