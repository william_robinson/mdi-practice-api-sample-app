'use strict';

describe('mdi-practice-api-sample-app.version module', function() {
  beforeEach(module('mdi-practice-api-sample-app.version'));

  describe('version service', function() {
    it('should return current version', inject(function(version) {
      expect(version).toEqual('0.1');
    }));
  });
});
