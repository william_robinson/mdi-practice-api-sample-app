'use strict';

angular.module('mdi-practice-api-sample-app.version', [
  'mdi-practice-api-sample-app.version.interpolate-filter',
  'mdi-practice-api-sample-app.version.version-directive'
])

.value('version', '0.1');
